//
//  MapView.swift
//  Landmarks
//
//  Created by 曹兴星 on 2019/12/4.
//  Copyright © 2019 曹兴星. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    
    var coordinate: CLLocationCoordinate2D
        
    /// Creates a `UIView` instance to be presented.
    func makeUIView(context: Self.Context) -> MKMapView {
        MKMapView(frame: .zero)
    }

    /// Updates the presented `UIView` (and coordinator) to the latest
    /// configuration.
    func updateUIView(_ uiView: MKMapView, context: Self.Context) {
        let span = MKCoordinateSpan(latitudeDelta: 2.0, longitudeDelta: 2.0)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        uiView.setRegion(region, animated: true)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(coordinate: landmarkData[0].locationCoordinate)
    }
}
