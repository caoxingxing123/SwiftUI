//
//  UserData.swift
//  Landmarks
//
//  Created by 曹兴星 on 2019/12/5.
//  Copyright © 2019 曹兴星. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

final class UserData: ObservableObject {
    
    @Published var showFavoritesOnly = false
    
    @Published var landmarks = landmarkData
    
    @Published var profile = Profile.default
    
}
