# SwiftUI
<img src="https://upload-images.jianshu.io/upload_images/4518631-9461af5baa32a89b.png" width="61.8%" height="33.3%" align=center>

#### 介绍
##### SwiftUI 初体验

仿SwiftUI 官方Tutorials，实现了一套列表和详情、用户信息的编辑和查看

建议：

      1.先浏览PPT:SwiftUI 初探.key

      2.然后看演示代码

#### 软件架构
MVC架构：

        Models      -->  数据模型

        Views       -->  简单视图/元视图/子视图

        Controllers -->  逻辑页面/业务页面

#### 软件、工具要求

1.  MacOS Catalina  10.15.x
2.  iOS   13.x
3.  Xcode 11.x

#### 页面说明

1.  首页
2.  详情页
3.  用户信息页
4.  用户编辑页

具体页面详看代码

#### 备注
代码地址：git@gitee.com:caoxingxing123/SwiftUI.git

欢迎issue 和 star
